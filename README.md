# [IF LER 01] Klassisches Maschinelles Lernen mit Python

## Voraussetzungen

### 1. Stelle sicher, dass du Python 3.6 oder 3.7 installiert hast, andernfalls installiere es
* `python --version` in der Kommandozeile zeigt dir - wenn installiert - die Version
* Windows: Download von https://www.python.org/downloads/, aktiviere die Option "Add Python to PATH"
* Mac: Download von https://www.python.org/downloads/ (oder via brew installieren)
* Linux:
    * Ubuntu 18.04 hat Python 3.6 vorinstalliert, es wird jedoch noch ein zusätzliches Paket benötigt:     
    `apt-get install python3-venv`

### 2. Jupyter innerhalb einer virtualenv installieren
* Lege ein neues Verzeichnis an und lege die [requirements.txt](https://gitlab.com/innoq/if2019-ml/raw/master/requirements.txt) dort ab (oder klone das Git Repository)
* Lege eine virtualenv innerhalb des Verzeichnisses an:
    * Abhängig von deinem System und zuvor installierte Python Versionen lautet das Kommando entweder python3 oder einfach python
    * `python3 -m venv venv`
    * `python -m venv venv`
* virtualenv aktivieren
    * Windows: `venv\Scripts\activate.bat`
    * Mac und Linux: `source venv/bin/activate`
    * Danach wird der Kommandozeile ein "(venv)" vorangestellt
* Jupyter und Machine Learning Bibliotheken installieren:
    * `pip install -r requirements.txt`

### 3. Jupyter starten
* Lade die Datei [vorbereitung.ipynb](https://gitlab.com/innoq/if2019-ml/raw/master/vorbereitung.ipynb) herunter (falls du das Git Repository geklont hast, hast du sie schon)
* `jupyter notebook`
* Danach sollte sich automatisch ein Browserfenster für Jupyter öffnen

## 4. Jupyter ausprobieren und Pythonkenntnisse auffrischen
* Klicke auf `vorbereitung.ipynb` dort findest du eine Anleitung
